#include "Arduino.h"
#include "src/epaper.h"
#include "src/my_clock.h"
#include "src/my_wifi.h"
#include "src/my_wifi_factory_settings.h"
#include "src/temp.h"
#include "src/sonar.h"

MyClock myClock;
MyWifi myWifi;
MyWifiFactorySettings myWifiFactorySettings;
Epaper epaper;
Temp temp;
Sonar sonar;
const unsigned int FACTORY_RESET_PIN = 19;

unsigned long getResetMilis(unsigned long resetMillis);
long getRealTimeWrapper();
void onWifiConnected(IPAddress ipAddress);
DynamicJsonDocument getStatus();
void onWifiDisconnected();
void onSynced();
void onOutOfSync();

void setup()
{
  Serial.begin(115200);
  myWifiFactorySettings.initEEPROM();
  pinMode(FACTORY_RESET_PIN, INPUT_PULLUP);
  delay(2000);
  if (digitalRead(FACTORY_RESET_PIN) == LOW)
  {
    myWifiFactorySettings.reset();
  }
  if (myWifiFactorySettings.isNeededBegin())
  {
    myWifiFactorySettings.begin();
    epaper.beginFactorySettings(myWifiFactorySettings.getApIpaddress().toString().c_str(), myWifiFactorySettings.getApSsid(), myWifiFactorySettings.getApPassword());
  }
  else
  {
    sonar.begin();
    epaper.setTemp((int)temp.getTemp());
    epaper.setFill(sonar.getTankLevel());
    epaper.begin();
    myWifi.begin(myWifiFactorySettings.getSsid(), myWifiFactorySettings.getPassword(), myWifiFactorySettings.getTimeSyncUri());
    myWifi.onWifiConnected = onWifiConnected;
    myWifi.onWifiDisconnected = onWifiDisconnected;
    myWifi.onOutOfSync = onOutOfSync;
    myWifi.onSynced = onSynced;
    myWifi.getStatus = getStatus;
    myClock.initialize(getRealTimeWrapper);
  }
}

void loop()
{
  if (myWifiFactorySettings.isNeeded())
  {
    myWifiFactorySettings.loop();
  }
  else
  {
    myWifi.loop();
  }
}

long getRealTimeWrapper()
{
  return myWifi.getRealTime();
}

void onWifiConnected(IPAddress ipAddress)
{
  epaper.setIpAddress(ipAddress.toString());
  epaper.wifiConnected();
}

DynamicJsonDocument getStatus()
{
  DynamicJsonDocument result(1024);
  result["temp"] = (int)temp.getTemp();
  result["tankLevel"] = sonar.getTankLevel();
  return result;
}

void onWifiDisconnected()
{
  epaper.wifiDisconnected();
}

void onSynced()
{
  epaper.inSync();
}

void onOutOfSync()
{
  epaper.outOfSync();
}
