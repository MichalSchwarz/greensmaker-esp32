#ifndef Clock_h
#define Clock_h

#include "Arduino.h"

class MyClock
{
public:
  void initialize(long (*getTimestamp)());
  int getHours();
  int getMinutes();
  int getSeconds();
};

#endif
