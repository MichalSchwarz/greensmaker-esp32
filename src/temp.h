#ifndef Temp_h
#define Temp_h

#include "Arduino.h"
#include <OneWire.h>
#include <DallasTemperature.h>

class Temp
{
private:
  const int ONE_WIRE_BUS = 25;
  OneWire oneWire;
  DallasTemperature dallasTempeature;

public:
  Temp();
  float getTemp();
};

#endif
