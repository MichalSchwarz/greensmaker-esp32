#include "Arduino.h"
#include "my_wifi_factory_settings.h"

MyWifiFactorySettings::MyWifiFactorySettings() : eepromPassword("password", MyWifiFactorySettings::PASSWORD_MAX_LENGTH + 1),
                                                 eepromSsid("ssid", MyWifiFactorySettings::SSID_MAX_LENGTH + 1),
                                                 eepromTimeSyncUri("timeSyncUri", MyWifiFactorySettings::TIME_SYNC_URI_MAX_LENGTH + 1),
                                                 eepromSetupDone("setupDone", 5)
{
}

void MyWifiFactorySettings::beginWebServer()
{
  this->server.begin(this->WEBSERVER_PORT);
  this->server.on("/", [this]() {
    server.send(200, "text/html", FACTORY_SETTINGS_HTML);
  });
  this->server.on("/save", [this]() {
    for (uint8_t i = 0; i < server.args(); i++)
    {
      Serial.println("for");
      if (server.argName(i) == "ssid")
      {
        this->eepromSsid.writeString(0, server.arg(i));
        this->eepromSsid.commit();
      }
      else if (server.argName(i) == "password")
      {
        this->eepromPassword.writeString(0, server.arg(i));
        this->eepromPassword.commit();
      }
      else if (server.argName(i) == "timeSyncUrl")
      {
        this->eepromTimeSyncUri.writeString(0, server.arg(i));
        this->eepromTimeSyncUri.commit();
      }
    }
    this->eepromSetupDone.writeString(0, "done");
    this->eepromSetupDone.commit();
    server.send(200, "text/html", FACTORY_SETTINGS_DONE_HTML);
    delay(2000);
    ESP.restart();
  });
}

void MyWifiFactorySettings::begin()
{
  this->generateRandomPassword(this->apPassword);
  WiFi.softAP(MyWifiFactorySettings::SSID_AP, this->apPassword);
  this->ipAddress = WiFi.softAPIP();
  this->beginWebServer();
}

void MyWifiFactorySettings::reset()
{
  this->eepromSetupDone.writeString(0, "");
  this->eepromSetupDone.commit();
  ESP.restart();
}

IPAddress MyWifiFactorySettings::getApIpaddress()
{
  return this->ipAddress;
}

const char *MyWifiFactorySettings::getApPassword()
{
  return this->apPassword;
}

const char *MyWifiFactorySettings::getApSsid()
{
  return MyWifiFactorySettings::SSID_AP;
}

const char *MyWifiFactorySettings::getSsid()
{
  return this->ssid;
}

const char *MyWifiFactorySettings::getPassword()
{
  return this->password;
}

const char *MyWifiFactorySettings::getTimeSyncUri()
{
  return this->timeSyncUri;
}

/**
 * inspired by https://stackoverflow.com/questions/440133/how-do-i-create-a-random-alpha-numeric-string-in-c/440240
 */
void MyWifiFactorySettings::generateRandomPassword(char *result)
{
  static const char alpha[] =
      "abcdefghijklmnopqrstuvwxyz";
  randomSeed(analogRead(0));
  for (int i = 0; i < MyWifiFactorySettings::RANDOM_PASSWORD_LENGTH; ++i)
  {
    result[i] = alpha[random(1024) % (sizeof(alpha) - 1)];
  }
  result[MyWifiFactorySettings::RANDOM_PASSWORD_LENGTH] = '\0';
}

void MyWifiFactorySettings::loop()
{
  this->server.handleClient();
}

bool MyWifiFactorySettings::isNeeded()
{
  return !this->isSetupDone;
}

bool MyWifiFactorySettings::isNeededBegin()
{
  eepromTimeSyncUri.readString(0, this->timeSyncUri, MyWifiFactorySettings::TIME_SYNC_URI_MAX_LENGTH);
  eepromPassword.readString(0, this->password, MyWifiFactorySettings::PASSWORD_MAX_LENGTH);
  eepromSsid.readString(0, this->ssid, MyWifiFactorySettings::SSID_MAX_LENGTH);
  eepromSetupDone.readString(0, this->setupDone, 4);
  if (strcmp(this->setupDone, "done") == 0)
  {
    this->isSetupDone = true;
  }
  return !this->isSetupDone;
}

void MyWifiFactorySettings::initEEPROM()
{
  if (!this->eepromPassword.begin(this->eepromPassword.length()) || !this->eepromSsid.begin(this->eepromSsid.length()) || !this->eepromTimeSyncUri.begin(this->eepromTimeSyncUri.length()) || !this->eepromSetupDone.begin(this->eepromSetupDone.length()))
  {
    delay(1000);
    ESP.restart();
  }
}
