#ifndef MyWifi_h
#define MyWifi_h

#include "Arduino.h"
#include "html/index.h"
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include <WebServer.h>

class MyWifi
{
private:
  const unsigned int WEBSERVER_PORT = 80;
  const char *ssid;
  const char *password;
  const char *timeSyncUri;
  bool isConnected = false;
  WebServer server;
  void connect();
  void disconnect();
  long parseTimeApi(String);
  void beginWebServer();

public:
  long getRealTime();
  void loop();
  void begin(const char *ssid, const char *password, const char *timeSyncUri);
  void (*onWifiConnected)(IPAddress ipAddress);
  DynamicJsonDocument (*getStatus)();
  void (*onWifiDisconnected)();
  void (*onOutOfSync)();
  void (*onSynced)();
};

#endif
