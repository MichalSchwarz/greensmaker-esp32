#include "Arduino.h"
#include "sonar.h"

Sonar::Sonar() : eepromSonarMin("sonarMin", sizeof(unsigned long)), eepromSonarMax("sonarMax", sizeof(unsigned long))
{
}

void Sonar::begin()
{
  if (!eepromSonarMin.begin(eepromSonarMin.length()) || !eepromSonarMax.begin(eepromSonarMax.length()))
  {
    delay(1000);
    ESP.restart();
  }
  eepromSonarMin.get(0, this->sonarMin);
  eepromSonarMax.get(0, this->sonarMax);
  pinMode(this->TRIG_PIN, OUTPUT);
  pinMode(this->ECHO_PIN, INPUT);
}

unsigned long Sonar::measure()
{
  digitalWrite(this->TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(this->TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(this->TRIG_PIN, LOW);
  return pulseIn(this->ECHO_PIN, HIGH);
}

void Sonar::saveMinToFlash(unsigned long newMin)
{
  this->sonarMin = newMin;
  this->eepromSonarMin.put(0, this->sonarMin);
  this->eepromSonarMin.commit();
}

void Sonar::saveMaxToFlash(unsigned long newMax)
{
  this->sonarMax = newMax;
  this->eepromSonarMax.put(0, this->sonarMax);
  this->eepromSonarMax.commit();
}

unsigned int Sonar::getTankLevel()
{
  unsigned int result = 100;
  unsigned long actualLevel = this->getDistance();
  if (actualLevel > this->sonarMax)
  {
    result = 0;
  }
  else if (this->sonarMin < actualLevel && actualLevel < this->sonarMax)
  {
    float range = this->sonarMax - this->sonarMin;
    if (range > 0)
    {
      float level = actualLevel - this->sonarMin;
      float ratio = level / range;
      result = (unsigned int)(100 - (100 * ratio));
    }
  }
  return result;
}

unsigned long Sonar::getDistance()
{
  std::map<unsigned long, unsigned long> values;
  for (size_t i = 0; i < this->MEASURE_COUNT; i++)
  {
    unsigned long value = this->measure();
    values[value] = values.count(value) ? values[value] + 1 : 1;
    delay(200);
  }
  unsigned long result = 0;
  size_t correct = 0;
  for (auto const &x : values)
  {
    if (x.second > correct)
    {
      correct = (size_t)x.second;
      result = (unsigned long)x.first;
    }
  }
  return result;
}
