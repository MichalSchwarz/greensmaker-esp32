#include "Arduino.h"
#include <WiFi.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include "my_wifi.h"

void MyWifi::begin(const char *ssid, const char *password, const char *timeSyncUri)
{
  this->ssid = ssid;
  this->password = password;
  this->timeSyncUri = timeSyncUri;
}

void MyWifi::connect()
{
  while (WiFi.status() != WL_CONNECTED)
  {
    WiFi.begin(this->ssid, this->password);
    delay(5000);
  }
  this->beginWebServer();
  if (this->onWifiConnected)
  {
    this->onWifiConnected(WiFi.localIP());
  }
}

void MyWifi::disconnect()
{
  WiFi.disconnect(true);
  if (this->onWifiDisconnected)
  {
    this->onWifiDisconnected();
  }
}

long MyWifi::parseTimeApi(String response)
{
  DynamicJsonDocument jsonBuffer(1024);
  deserializeJson(jsonBuffer, response);
  long time = jsonBuffer["timestamp"];
  return time;
}

void MyWifi::beginWebServer()
{
  this->server.begin(this->WEBSERVER_PORT);
  this->server.on("/", [this]() {
    server.send(200, "text/html", INDEX_HTML);
  });
  this->server.on("/status", [this]() {
    DynamicJsonDocument status = getStatus();
    server.send(200, "text/json", status.as<String>());
  });
}

void MyWifi::loop()
{
  this->server.handleClient();
}

long MyWifi::getRealTime()
{
  long result;
  if (this->onOutOfSync)
  {
    this->onOutOfSync();
  }
  this->connect();
  HTTPClient http;
  http.begin(this->timeSyncUri);
  http.GET();
  String payload = http.getString();
  result = parseTimeApi(payload);
  http.end();
  if (this->onSynced && result)
  {
    this->onSynced();
  }
  return result;
}
