#ifndef MyWifiFactorySettings_h
#define MyWifiFactorySettings_h

#include "Arduino.h"
#include "html/factory_settings.h"
#include "html/factory_settings_done.h"
#include <EEPROM.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <WebServer.h>

class MyWifiFactorySettings
{
private:
  static const unsigned int SSID_MAX_LENGTH = 32;
  static const unsigned int PASSWORD_MAX_LENGTH = 64;
  static const unsigned int TIME_SYNC_URI_MAX_LENGTH = 256;
  static const unsigned int RANDOM_PASSWORD_LENGTH = 8;
  static const unsigned int WEBSERVER_PORT = 80;
  static constexpr const char *SSID_AP = "GREENSMAKER";
  void beginWebServer();
  void generateRandomPassword(char *);
  EEPROMClass eepromSsid;
  EEPROMClass eepromPassword;
  EEPROMClass eepromTimeSyncUri;
  EEPROMClass eepromSetupDone;
  char ssid[SSID_MAX_LENGTH + 1];
  char password[PASSWORD_MAX_LENGTH + 1];
  char timeSyncUri[TIME_SYNC_URI_MAX_LENGTH + 1];
  char setupDone[5];
  char apPassword[RANDOM_PASSWORD_LENGTH + 1];
  IPAddress ipAddress;
  bool isSetupDone = false;
  WebServer server;

public:
  MyWifiFactorySettings();
  bool isNeeded();
  bool isNeededBegin();
  void initEEPROM();
  void loop();
  void begin();
  void reset();
  const char *getApSsid();
  const char *getApPassword();
  const char *getSsid();
  const char *getPassword();
  const char *getTimeSyncUri();
  IPAddress getApIpaddress();
};

#endif
