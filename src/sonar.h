#ifndef Sonar_h
#define Sonar_h

#include "Arduino.h"
#include <map>
#include <EEPROM.h>

class Sonar
{
private:
  const int TRIG_PIN = 13;
  const int ECHO_PIN = 12;
  const int MEASURE_COUNT = 5;
  EEPROMClass eepromSonarMin;
  EEPROMClass eepromSonarMax;
  unsigned long sonarMin = 0;
  unsigned long sonarMax = 0;
  unsigned long measure();
  void saveMinToFlash(unsigned long newMin);
  void saveMaxToFlash(unsigned long newMax);

public:
  Sonar();
  void begin();
  unsigned long getDistance();
  unsigned int getTankLevel();
};

#endif
