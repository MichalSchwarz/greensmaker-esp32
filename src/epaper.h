/*
 * This file is part of the  distribution (https://github.com/wifi-drone-esp32 or http://wifi-drone-esp32.github.io).
 * Copyright (c) 2019 Michal Schwarz.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef Epaper_h
#define Epaper_h

#include "Arduino.h"
#include <SPI.h>
#include "lib/epd/epd2in13b.h"
#include "lib/epd/epdpaint.h"

#define COLORED 0
#define UNCOLORED 1

#define CHECKBOX_SIZE 14

class Epaper
{
private:
  bool isWifi = false;
  bool isSync = false;
  String ipAddress;
  int temp = 0;
  int fill = 0;
  Epd epd;
  unsigned char image[4096];
  Paint paint;
  void printWholeScreen();
  void printWifiBlack();
  void printWifiRed();
  void printFrame();
  void printSyncBlack();
  void printSyncRed();
  void printTempBlack();
  void printFillBlack();
  void printIpAddressBlack();
  void printFactorySettings(const char *apIpAddress, const char *apIpSsid, const char *apPassword);

public:
  Epaper();
  void begin();
  void beginFactorySettings(const char *apIpAddress, const char *apIpSsid, const char *apPassword);
  void wifiConnected();
  void wifiDisconnected();
  void inSync();
  void outOfSync();
  void setTemp(int temp);
  void setFill(int fill);
  void setIpAddress(String ipAddress);
};

#endif
