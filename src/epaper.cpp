#include "Arduino.h"
#include "epaper.h"

Epaper::Epaper()
    : paint(this->image, this->epd.width, this->epd.height)
{
  this->paint.SetRotate(ROTATE_90);
}

void Epaper::begin(void)
{
  this->printWholeScreen();
}

void Epaper::beginFactorySettings(const char *apIpAddress, const char *apIpSsid, const char *apPassword)
{
  this->printFactorySettings(apIpAddress, apIpSsid, apPassword);
}

void Epaper::printWholeScreen(void)
{
  this->epd.Init();
  this->epd.ClearFrame();
  this->paint.Clear(UNCOLORED);
  this->printWifiRed();
  this->printSyncRed();
  this->epd.SetPartialWindowRed(this->paint.GetImage(), 0, 0, this->paint.GetWidth(), this->paint.GetHeight());
  this->printWifiBlack();
  this->printSyncBlack();
  this->printTempBlack();
  this->printFillBlack();
  this->printIpAddressBlack();
  this->printFrame();
  this->epd.SetPartialWindowBlack(this->paint.GetImage(), 0, 0, this->paint.GetWidth(), this->paint.GetHeight());
  this->epd.DisplayFrame();
  this->epd.Sleep();
}

void Epaper::printFactorySettings(const char *apIpAddress, const char *apIpSsid, const char *apPassword)
{
  this->epd.Init();
  this->epd.ClearFrame();
  this->paint.Clear(UNCOLORED);
  this->paint.DrawStringAt(5, 5, "Please connect to:", &Font16, COLORED);
  this->paint.DrawStringAt(5, 25, "WiFi: ", &Font16, COLORED);
  this->paint.DrawStringAt(64, 25, apIpSsid, &Font16, COLORED);
  this->paint.DrawStringAt(5, 45, "Password: ", &Font16, COLORED);
  this->paint.DrawStringAt(105, 45, apPassword, &Font16, COLORED);
  this->paint.DrawStringAt(5, 65, "Then go to:", &Font16, COLORED);
  this->paint.DrawStringAt(5, 85, "http://", &Font16, COLORED);
  this->paint.DrawStringAt(84, 85, apIpAddress, &Font16, COLORED);
  this->epd.SetPartialWindowBlack(this->paint.GetImage(), 0, 0, this->paint.GetWidth(), this->paint.GetHeight());
  this->epd.DisplayFrame();
  this->epd.Sleep();
}

void Epaper::printWifiRed()
{
  int startX = 19;
  int startY = 8;
  int center = (CHECKBOX_SIZE / 2);
  if (this->isWifi)
  {
    this->paint.DrawLine(startX + 2, startY + center, startX + center - 2, startY + CHECKBOX_SIZE - 2, COLORED);
    this->paint.DrawLine(startX + 2 + 1, startY + center, startX + center - 2 + 1, startY + CHECKBOX_SIZE - 2, COLORED);
    this->paint.DrawLine(startX + center - 2, startY + CHECKBOX_SIZE - 2, startX + CHECKBOX_SIZE - 2, startY + (CHECKBOX_SIZE / 4), COLORED);
    this->paint.DrawLine(startX + center - 2 + 1, startY + CHECKBOX_SIZE - 2, startX + CHECKBOX_SIZE - 2 + 1, startY + (CHECKBOX_SIZE / 4), COLORED);
  }
}

void Epaper::printWifiBlack()
{
  int startX = 19;
  int startY = 8;
  int center = (CHECKBOX_SIZE / 2);
  this->paint.DrawStringAt(5, 30, "WiFi", &Font16, COLORED);
  this->paint.DrawRectangle(startX, startY, startX + CHECKBOX_SIZE, startY + CHECKBOX_SIZE, COLORED);
}

void Epaper::printSyncRed()
{
  int startX = 73;
  int startY = 8;
  int center = (CHECKBOX_SIZE / 2);
  if (this->isSync)
  {
    this->paint.DrawLine(startX + 2, startY + center, startX + center - 2, startY + CHECKBOX_SIZE - 2, COLORED);
    this->paint.DrawLine(startX + 2 + 1, startY + center, startX + center - 2 + 1, startY + CHECKBOX_SIZE - 2, COLORED);
    this->paint.DrawLine(startX + center - 2, startY + CHECKBOX_SIZE - 2, startX + CHECKBOX_SIZE - 2, startY + (CHECKBOX_SIZE / 4), COLORED);
    this->paint.DrawLine(startX + center - 2 + 1, startY + CHECKBOX_SIZE - 2, startX + CHECKBOX_SIZE - 2 + 1, startY + (CHECKBOX_SIZE / 4), COLORED);
  }
}

void Epaper::printSyncBlack()
{
  int startX = 73;
  int startY = 8;
  int center = (CHECKBOX_SIZE / 2);
  this->paint.DrawStringAt(58, 30, "Sync", &Font16, COLORED);
  this->paint.DrawRectangle(startX, startY, startX + CHECKBOX_SIZE, startY + CHECKBOX_SIZE, COLORED);
}

void Epaper::printTempBlack()
{
  this->paint.DrawStringAt(112, 30, "Temp", &Font16, COLORED);
  String temp(this->temp);
  this->paint.DrawStringAt(117, 7, temp.c_str(), &Font24, COLORED);
}

void Epaper::printFillBlack()
{
  this->paint.DrawStringAt(166, 30, "Fill", &Font16, COLORED);
  String fill(this->fill);
  this->paint.DrawStringAt(166, 7, fill.c_str(), &Font24, COLORED);
}

void Epaper::printIpAddressBlack()
{
  this->paint.DrawStringAt(10, 60, this->ipAddress.c_str(), &Font12, COLORED);
}

void Epaper::printFrame()
{
  int boxX = this->paint.GetHeight() / 4;
  int boxY = 50;
  this->paint.DrawRectangle(0, boxY, this->paint.GetHeight(), boxY, COLORED);
  this->paint.DrawRectangle(boxX, 0, boxX, boxY, COLORED);
  this->paint.DrawRectangle(boxX * 2, 0, boxX * 2, boxY, COLORED);
  this->paint.DrawRectangle(boxX * 3, 0, boxX * 3, boxY, COLORED);
}

void Epaper::wifiConnected(void)
{
  this->isWifi = true;
  this->printWholeScreen();
}

void Epaper::wifiDisconnected(void)
{
  this->isWifi = false;
  this->printWholeScreen();
}

void Epaper::inSync(void)
{
  this->isSync = true;
  this->printWholeScreen();
}

void Epaper::outOfSync(void)
{
  this->isSync = false;
  this->printWholeScreen();
}

void Epaper::setTemp(int temp)
{
  this->temp = temp;
}

void Epaper::setFill(int fill)
{
  this->fill = fill;
}

void Epaper::setIpAddress(String ipAddress)
{
  this->ipAddress = ipAddress;
}
