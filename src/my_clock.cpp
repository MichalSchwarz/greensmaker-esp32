#include "Arduino.h"
#include <Time.h>
#include "my_clock.h"

void MyClock::initialize(long (*getTimestamp)())
{
  setSyncProvider(getTimestamp);
  setSyncInterval(86400);
  while (timeStatus() != timeSet)
  {
    delay(10000);
  }
}

int MyClock::getHours()
{
  return hour();
}

int MyClock::getMinutes()
{
  return minute();
}

int MyClock::getSeconds()
{
  return second();
}
