#include "Arduino.h"
#include "temp.h"

Temp::Temp() : oneWire(this->ONE_WIRE_BUS), dallasTempeature(&this->oneWire)
{
}

float Temp::getTemp()
{
  this->dallasTempeature.requestTemperatures();
  return dallasTempeature.getTempCByIndex(0);
}
