#ifndef Factory_settings_h
#define Factory_settings_h

const char FACTORY_SETTINGS_HTML[] PROGMEM = "<!DOCTYPE html><html><head><title>Setup</title><link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\"><meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\"/></head><body><h1>Greensmaker setup</h1><form action=\"/save\" method=\"POST\"><br><input type=\"text\" name=\"ssid\" placeholder=\"ssid\" required><br><input type=\"password\" name=\"password\" placeholder=\"password\" required><br><input type=\"text\" name=\"timeSyncUrl\" placeholder=\"timeSyncUrl\" required><br><input type=\"submit\" name=\"send\" value=\"Send\"></form></body></html>";

#endif
